'use strict';

const fs = require('fs');




function recTime(timeMedio, name){
    var d=new Date();
    var minutes=parseInt(timeMedio.split(":")[0]);
    var seconds=parseInt(timeMedio.split(":")[1]);
    var hour=d.getHours();
    seconds=seconds+d.getSeconds();
    if(seconds>=60){
        seconds=seconds-60;
        minutes=minutes+1;
    }
    minutes=minutes+d.getMinutes();
    if(minutes>=60){
        minutes=minutes-60;
        hour=hour+1;
    }

    var myEnd=hour+":"+minutes+":"+seconds;

    console.log("Rectime agora:"+ myEnd);
    let coffee={
        responsable: name,
        day: d.getUTCDate()+"/"+(d.getUTCMonth())+"/"+d.getUTCFullYear(),
        hourBegin:d.getHours()+":"+d.getMinutes()+":"+d.getSeconds(),
        hourConclusion: myEnd
    }
    let data = JSON.stringify(coffee);  
    fs.writeFileSync('database/Coffee.json', data);
}

function readTime(myFile){
    //const fs = require('fs');
    fs.readFile(myFile, (err, data)=>{
        if(err) throw err;
        let coffee = JSON.parse(data);
        console.log(coffee);
    });
}

function checkTime(myFile){
   
    var d = new Date();
    var currentSeconds;
    var finalsSeconds;
    var remaingSeconds;
    var currentTime;
    var remainingTime;
    fs.readFile(myFile, (err, data)=>{
        if(err) throw err;
        let coffee = JSON.parse(data);
        console.log(coffee.day);
        console.log("Hora Conclusão:"+coffee.hourConclusion);
        var hour=d.getHours();
        var minute=d.getMinutes();
        var second=d.getSeconds();
        finalsSeconds=(parseInt(coffee.hourConclusion.split(":")[0])*3600)+(parseInt(coffee.hourConclusion.split(":")[1])*60)+(parseInt(coffee.hourConclusion.split(":")[2]));
        currentSeconds=(hour*3600)+(minute*60)+second;
        currentTime=hour+":"+minute+":"+second;
        console.log("Segundos atuais:"+currentSeconds);
        console.log("Segundos Finais:"+finalsSeconds);
        console.log("Tempo restante: "+currentTime);
        if(finalsSeconds<=currentSeconds){
            console.log("Tempo Estourado");

        }
        else{
            var timeRemaining;
            var remainingSeconds=parseInt(finalsSeconds-currentSeconds);
            console.log("Segundos restantes:" +remainingSeconds);
            timeRemaining=(remainingSeconds)/3600;
        }
    });
}

//recTime("8:00", 'Rodrigo k. Ben');
//readTime('database/Coffee.json');
checkTime('database/Coffee.json');